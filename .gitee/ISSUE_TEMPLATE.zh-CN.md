### 任务登记
#### 标题
##### 普通任务
标签:
[Update/New]

主标题:
 更新/新增xxx应用名_v版本号

##### Bug类
标签:
[Bug]

主标题:
 xxx应用名_v版本号存在xx问题

##### 心愿单
标签:
[Wishlist][Update/New]

主标题:
1. 更新/新增xxx应用名_v版本号
2. xxx应用名_v版本号xxx功能异常反馈

#### Issue标签
  根据标题标签以及应用信息为Issue设置标签,目前支持的tags:

1. 支持的系统
```
deepin20
deepin23
UOS20
```

2. 支持的架构平台
```
amd64
aarch64
all
```

3. Issue类型
```
update
new
bug
wishlist
```


#### 基本信息
```
App-name: ""
Pkg-mode: "deb"
Arch: ""
Pkg-name: ""
Version: ""
Homepage: ""
Maintainer: ""
```



#### 来源
  eg. 填写该issue来源情况，可填链接、截图或直接引用其他Issue、PR


#### 问题详细描述(Bug必填)


#### 其他备注信息




